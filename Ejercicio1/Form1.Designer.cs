﻿namespace Ejercicio1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.julio = new System.Windows.Forms.Button();
            this.nico = new System.Windows.Forms.Button();

            this.btnNicoEdit = new System.Windows.Forms.Button();
            this.txtNico = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1.SuspendLayout();
            this.pcambiador = new System.Windows.Forms.TextBox();
            this.pcambiador2 = new System.Windows.Forms.TextBox();
            this.pcambiador3 = new System.Windows.Forms.TextBox();
            this.plabel1 = new System.Windows.Forms.Label();
            this.plabel2 = new System.Windows.Forms.Label();
            this.plabel3 = new System.Windows.Forms.Label();
            this.pcheckBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(383, 62);
            this.button1.Location = new System.Drawing.Point(613, 100);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Pere";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(273, 62);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 1;
            this.button2.Text = "Adrián";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(273, 97);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 28);
            this.button3.TabIndex = 1;
            this.button3.Text = "Edit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.EditAdrian_Click);
            // 
            // julio
            // 

            this.julio.BackColor = System.Drawing.Color.IndianRed;
            this.julio.Location = new System.Drawing.Point(8, 32);
            this.julio.Location = new System.Drawing.Point(160, 81);
            this.julio.Margin = new System.Windows.Forms.Padding(2);
            this.julio.Name = "julio";
            this.julio.Size = new System.Drawing.Size(107, 62);
            this.julio.TabIndex = 2;
            this.julio.Text = "Julio";
            this.julio.UseVisualStyleBackColor = false;
            this.julio.Click += new System.EventHandler(this.button3_Click);
            // 
            // nico
            // 
            this.nico.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.nico.Location = new System.Drawing.Point(40, 100);
            this.nico.Margin = new System.Windows.Forms.Padding(4);
            this.nico.Name = "nico";

            this.nico.Size = new System.Drawing.Size(75, 23);
            this.nico.TabIndex = 3;
            this.nico.Text = "Nicolas Vanderlande ES";
            this.nico.UseVisualStyleBackColor = false;
            this.nico.Click += new System.EventHandler(this.nico_Click);
            // 
            // btnNicoEdit
            // 
            this.btnNicoEdit.BackColor = System.Drawing.Color.Orange;
            this.btnNicoEdit.Location = new System.Drawing.Point(40, 121);
            this.btnNicoEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnNicoEdit.Name = "btnNicoEdit";
            this.btnNicoEdit.Size = new System.Drawing.Size(98, 23);
            this.btnNicoEdit.TabIndex = 4;
            this.btnNicoEdit.Text = "Edit";
            this.btnNicoEdit.UseVisualStyleBackColor = false;
            this.btnNicoEdit.Click += new System.EventHandler(this.btnNicoEdit_Click);
            // 
            // txtNico
            // 
            this.txtNico.Location = new System.Drawing.Point(40, 142);
            this.txtNico.Name = "txtNico";
            this.txtNico.Size = new System.Drawing.Size(110, 20);
            this.txtNico.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(613, 31);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            // pcambiador
            // 
            this.pcambiador.Location = new System.Drawing.Point(598, 129);
            this.pcambiador.Name = "pcambiador";
            this.pcambiador.Size = new System.Drawing.Size(115, 20);
            this.pcambiador.TabIndex = 4;
            // 
            // pcambiador2
            // 
            this.pcambiador2.Location = new System.Drawing.Point(597, 174);
            this.pcambiador2.Name = "pcambiador2";
            this.pcambiador2.Size = new System.Drawing.Size(116, 20);
            this.pcambiador2.TabIndex = 5;
            // 
            // pcambiador3
            // 
            this.pcambiador3.Location = new System.Drawing.Point(597, 218);
            this.pcambiador3.Name = "pcambiador3";
            this.pcambiador3.Size = new System.Drawing.Size(115, 20);
            this.pcambiador3.TabIndex = 6;
            // 
            // plabel1
            // 
            this.plabel1.AutoSize = true;
            this.plabel1.Location = new System.Drawing.Point(548, 132);
            this.plabel1.Name = "plabel1";
            this.plabel1.Size = new System.Drawing.Size(44, 13);
            this.plabel1.TabIndex = 7;
            this.plabel1.Text = "Nombre";
            // 
            // plabel2
            // 
            this.plabel2.AutoSize = true;
            this.plabel2.Location = new System.Drawing.Point(548, 177);
            this.plabel2.Name = "plabel2";
            this.plabel2.Size = new System.Drawing.Size(32, 13);
            this.plabel2.TabIndex = 8;
            this.plabel2.Text = "Edad";
            // 
            // plabel3
            // 
            this.plabel3.AutoSize = true;
            this.plabel3.Location = new System.Drawing.Point(548, 221);
            this.plabel3.Name = "plabel3";
            this.plabel3.Size = new System.Drawing.Size(23, 13);
            this.plabel3.TabIndex = 9;
            this.plabel3.Text = "title";
            // 
            // pcheckBox1
            // 
            this.pcheckBox1.AutoSize = true;
            this.pcheckBox1.Location = new System.Drawing.Point(568, 270);
            this.pcheckBox1.Name = "pcheckBox1";
            this.pcheckBox1.Size = new System.Drawing.Size(48, 17);
            this.pcheckBox1.TabIndex = 10;
            this.pcheckBox1.Text = "Nolo";
            this.pcheckBox1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 202);
            this.Controls.Add(this.txtNico);
            this.Controls.Add(this.btnNicoEdit);
            this.ClientSize = new System.Drawing.Size(768, 380);
            this.Controls.Add(this.pcheckBox1);
            this.Controls.Add(this.plabel3);
            this.Controls.Add(this.plabel2);
            this.Controls.Add(this.plabel1);
            this.Controls.Add(this.pcambiador3);
            this.Controls.Add(this.pcambiador2);
            this.Controls.Add(this.pcambiador);
            this.Controls.Add(this.nico);
            this.Controls.Add(this.julio);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button julio;
        private System.Windows.Forms.Button nico;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.Button btnNicoEdit;
        private System.Windows.Forms.TextBox txtNico;
        private System.Windows.Forms.TextBox pcambiador;
        private System.Windows.Forms.TextBox pcambiador2;
        private System.Windows.Forms.TextBox pcambiador3;
        private System.Windows.Forms.Label plabel1;
        private System.Windows.Forms.Label plabel2;
        private System.Windows.Forms.Label plabel3;
        private System.Windows.Forms.CheckBox pcheckBox1;

    }
}